import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Model {
	
	 private final ObservableList<String> list =  FXCollections.observableArrayList(); 
	 
	 
		
	 public Model() {


		
	}

	public ObservableList<String> Items() {
		  return list; 
		  } 
	
	public boolean add(String user){
		
		return list.add(user);
		
	}

	
	public String[] getData() {
		
		String[] tab_user = new String[list.size()];
		tab_user=list.toArray(tab_user);
				return tab_user;
	}
	

public boolean isSorted()
{
    for(int a=0;a<list.size()-1;a++)
    {
        if(list.get(a).compareTo(list.get(a+1))>0)
        {
            return false;
        }
    }
    return true;
}

	@Override
	public String toString() {
	
		   StringBuilder strbuild = new StringBuilder("Liste User Model:" + System.lineSeparator());
		for(int i=0;i<list.size();i++){
			strbuild.append(i + ": " + list.get(i).toString()+ System.lineSeparator());
		}
			
		return strbuild.toString();
	}

}
